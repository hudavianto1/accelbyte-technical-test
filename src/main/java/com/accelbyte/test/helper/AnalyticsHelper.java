package com.accelbyte.test.helper;

import static com.accelbyte.test.constant.Constants.PATTERN;
import static com.accelbyte.test.constant.Constants.REGEX_MINUTE;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.time.DateUtils;

/**
 * @author hudaavianto
 */
public class AnalyticsHelper {

  private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(PATTERN);

  public static Date logDate(String result) {

    try {
      String[] split = result.split(REGEX_MINUTE);
      String time = split[0].concat(" ").concat(split[1]).concat(":").concat(split[2]);
      Date dateLog = DATE_FORMAT.parse(time);
      return dateLog;
    } catch (Exception e) {
      return null;
    }
  }

  public static InputStream getInputStream(String filepath) throws IOException {
    return new FileInputStream(filepath);
  }

  public static void streamValue(List<String> stream, String time) {
    String str = stream.get(0);
    String result = str.substring(str.indexOf("[") + 1, str.indexOf("]"));
    Date paramDate = logDate(result);
    Date range = DateUtils.addMinutes(new Date(), -Integer.parseInt(time));

    if (checkFirstValue(paramDate, range)) {
      stream.forEach(strs -> {
        if (paramDate.before(new Date()) && paramDate.after(range)) {
          System.out.println(str);
        }
      });
    }
  }

  public static boolean checkFirstValue(Date paramDate, Date range) {
    if (!paramDate.before(new Date()) && !paramDate.after(range)) {
      return false;
    }
    return true;
  }

}
