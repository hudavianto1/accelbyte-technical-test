package com.accelbyte.test.constant;

/**
 * @author hudaavianto
 */
public class Constants {

  public static final String PATTERN = "dd/MMM/yyyy HH:mm";
  public static final String REGEX_MINUTE = ":";
  public static final String CHARSET = "UTF-8";

}
