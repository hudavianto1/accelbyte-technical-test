package com.accelbyte.test.entity;

import picocli.CommandLine.Option;

/**
 * @author hudaavianto
 */
public class Analytics {

  @Option(names = {"-t", "--time"}, required = true, description = "time in minutes.")
  public String time;

  @Option(names = {"-d", "--directory"}, required = true, description = "Dir location.")
  public String directory;

}
