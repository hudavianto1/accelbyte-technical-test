package com.accelbyte.test;

import static com.accelbyte.test.constant.Constants.CHARSET;
import static com.accelbyte.test.helper.AnalyticsHelper.getInputStream;
import static com.accelbyte.test.helper.AnalyticsHelper.streamValue;

import com.accelbyte.test.entity.Analytics;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import picocli.CommandLine;
import picocli.CommandLine.Command;

/**
 * @author hudaavianto
 */

@Command(
    name = "analytics",
    description = "analytics"
)
@SpringBootApplication
public class AnalyticsDemo extends Analytics implements Callable<Integer> {

  public static void main(String[] args) {
    int exitCode = new CommandLine(new AnalyticsDemo()).execute(args);
    System.exit(exitCode);
    SpringApplication.run(AnalyticsDemo.class, args);
  }

  @Override
  public Integer call() {
    getAnalytics();
    return 0;
  }

  public void getAnalytics() {
    int n = 1;
    do {
      try (InputStreamReader input = new InputStreamReader(
          getInputStream(directory.concat("http-" + n + ".log")),
          Charset.forName(CHARSET))) {

        Stream<String> stream = new BufferedReader(input).lines();
        List<String> streamToList = stream.collect(Collectors.toList());

        streamValue(streamToList, time.replaceAll("[^0-9]", ""));
        n++;
      } catch (IOException e) {
        return;
      }
    } while (n > 1);
  }


}
